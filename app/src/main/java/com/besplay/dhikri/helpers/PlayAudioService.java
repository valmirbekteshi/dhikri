package com.besplay.dhikri.helpers;

import android.app.Service;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.widget.ImageButton;

import com.besplay.dhikri.R;
import com.besplay.dhikri.adapter.DuaAdapter;
import com.besplay.dhikri.model.Dua;

import java.io.IOException;
import java.util.List;

/**
 * Created by Valmir on 5/25/2017.
 */

public class PlayAudioService extends Service {

    public static MediaPlayer mMediaPlayer;
    public static PlayAudioService playAudioService;
    private ImageButton playPauseButton;
    public Dua duaService;


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        playAudioService = this;
        mMediaPlayer = new MediaPlayer();
        mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        return super.onStartCommand(intent, flags, startId);
    }


    public static void Pause() {
        mMediaPlayer.pause();
        mMediaPlayer.release();
    }




    public void Play( List<Dua> duaArrayList, Dua dua, Uri uri, final ImageButton imgButton, DuaAdapter duaAdapter) {

        playPauseButton = imgButton;


        if (mMediaPlayer.isPlaying()) {
            if (duaService.getId() == dua.getId()) {
                mMediaPlayer.pause();

                playPauseButton.setBackgroundResource(R.drawable.play_icon);
            } else {

                new UpdateList(duaArrayList, duaAdapter, dua, uri).execute();
                //PlayAudio(dua, uri);
            }

        } else {

            //PlayAudio(dua, uri);
            new UpdateList( duaArrayList, duaAdapter, dua, uri).execute();
        }


        duaService = dua;
    }

    public class UpdateList extends AsyncTask<Dua, Uri, Void> {

        Dua dua;
        Uri uri;
        List<Dua> duaList;
        DuaAdapter duaAdapter;

        public UpdateList(List<Dua> duaList, DuaAdapter duaAdapter, Dua dua, Uri uri) {
            this.dua = dua;
            this.uri = uri;
            this.duaList = duaList;
            this.duaAdapter = duaAdapter;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            for (Dua duaItem : duaList) {
                duaItem.setPressed(false);
            }
        }

        @Override
        protected Void doInBackground(Dua... params) {
            try {
                dua.setPressed(true);

                mMediaPlayer.release();
                mMediaPlayer = new MediaPlayer();

                mMediaPlayer.setDataSource(PlayAudioService.this, uri);

                mMediaPlayer.prepareAsync();

            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            mMediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                public void onPrepared(MediaPlayer mp) {
                    try {
                        mMediaPlayer.start();
                    } catch (Exception e) {
                    }
                }
            });

            mMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {


                    playPauseButton.setBackgroundResource(R.drawable.icon_play);
                    mMediaPlayer.reset();

                }
            });

            duaAdapter.notifyDataSetChanged();

            playPauseButton.setBackgroundResource(R.drawable.pause_icon);


        }
    }


    public static boolean isPlaying() {
        if (mMediaPlayer == null) {
            return false;
        } else {
            return mMediaPlayer.isPlaying();
        }

    }


    @Override
    public void onCreate() {
        super.onCreate();


    }


}
