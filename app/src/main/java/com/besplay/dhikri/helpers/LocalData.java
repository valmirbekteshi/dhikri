package com.besplay.dhikri.helpers;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Jaison on 18/06/17.
 */

public class LocalData {

    private static final String APP_SHARED_PREFS = "DhikriPref";

    private SharedPreferences appSharedPrefs;
    private SharedPreferences.Editor prefsEditor;

    private static final String amReminderStatus = "amReminderStatus";
    private static final String pmReminderStatus = "pmReminderStatus";

    private static final String amhour = "amhour";
    private static final String pmhour = "pmhour";

    private static final String ammin = "ammin";
    private static final String pmmin = "pmmin";

    private static final String amNotificationId = "amNotificationId";
    private static final String pmNotificationId = "pmNotificationId";

    public LocalData(Context context) {
        this.appSharedPrefs = context.getSharedPreferences(APP_SHARED_PREFS, Context.MODE_PRIVATE);
        this.prefsEditor = appSharedPrefs.edit();
    }

    // Settings Page Set Reminder

    public boolean getAmReminderStatus() {
        return appSharedPrefs.getBoolean(amReminderStatus, false);
    }

    public boolean getPmReminderStatus() {
        return appSharedPrefs.getBoolean(pmReminderStatus, false);
    }

    public void setAmReminderStatus(boolean status) {
        prefsEditor.putBoolean(amReminderStatus, status);
        prefsEditor.commit();
    }

    public void setPmReminderStatus(boolean status) {
        prefsEditor.putBoolean(pmReminderStatus, status);
        prefsEditor.commit();
    }

    // Settings Page Reminder Time (Hour)

    public int get_AmHour() {
        return appSharedPrefs.getInt(amhour, 20);
    }

    public void set_AmHour(int h) {
        prefsEditor.putInt(amhour, h);
        prefsEditor.commit();
    }

    public int get_PmHour() {
        return appSharedPrefs.getInt(pmhour, 20);
    }

    public void set_PmHour(int h) {
        prefsEditor.putInt(pmhour, h);
        prefsEditor.commit();
    }


    // Settings Page Reminder Time (Minutes)

    public int get_AmMin() {
        return appSharedPrefs.getInt(ammin, 0);
    }

    public void set_AmMin(int m) {
        prefsEditor.putInt(ammin, m);
        prefsEditor.commit();
    }


    public int get_PmMin() {
        return appSharedPrefs.getInt(pmmin, 0);
    }

    public void set_PmMin(int m) {
        prefsEditor.putInt(pmmin, m);
        prefsEditor.commit();
    }

    public void setAmNotificationId(int id) {
        prefsEditor.putInt(amNotificationId, id);
        prefsEditor.commit();
    }

    public int getAmNotificationId() {
        return appSharedPrefs.getInt(amNotificationId, 0);
    }


    public void setPmNotificationId(int id) {
        prefsEditor.putInt(pmNotificationId, id);
        prefsEditor.commit();
    }

    public int getPmNotificationId() {
        return appSharedPrefs.getInt(pmNotificationId, 0);
    }


    public void reset() {
        prefsEditor.clear();
        prefsEditor.commit();

    }


}
