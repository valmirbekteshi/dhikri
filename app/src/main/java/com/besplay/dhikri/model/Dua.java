package com.besplay.dhikri.model;

import java.io.Serializable;

/**
 * Created by Valmir on 5/17/2017.
 */

public class Dua implements Serializable {

    private int id;

    private int audioResourceId;
    private String duaTitle;
    private String duaTranslation;
    private boolean isPressed;

    public Dua(int id, int audioResourceId, String duaTitle, String duaTranslation, String duaReference, boolean isPressed) {
        this.id = id;
        this.audioResourceId = audioResourceId;
        this.duaTitle = duaTitle;
        this.duaTranslation = duaTranslation;
        this.duaReference = duaReference;
        this.isPressed = isPressed;
    }

    public int getAudioResourceId() {
        return audioResourceId;
    }

    public void setAudioResourceId(int audioResourceId) {
        this.audioResourceId = audioResourceId;
    }

    public String getDuaTitle() {
        return duaTitle;
    }

    public void setDuaTitle(String duaTitle) {
        this.duaTitle = duaTitle;
    }

    public String getDuaTranslation() {
        return duaTranslation;
    }

    public void setDuaTranslation(String duaTranslation) {
        this.duaTranslation = duaTranslation;
    }

    public String getDuaReference() {
        return duaReference;
    }

    public void setDuaReference(String duaReference) {
        this.duaReference = duaReference;
    }


    public int getId() {
        return id;
    }




    public void setId(int id) {
        this.id = id;
    }


    private String duaReference;

    public boolean isPressed() {
        return isPressed;
    }

    public void setPressed(boolean pressed) {
        isPressed = pressed;
    }
}