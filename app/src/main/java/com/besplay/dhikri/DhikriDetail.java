package com.besplay.dhikri;

import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.besplay.dhikri.helpers.PlayAudioService;
import com.besplay.dhikri.model.Dua;
import com.google.gson.Gson;

import java.io.File;
import java.io.IOException;

public class DhikriDetail extends AppCompatActivity {

    String dua_title;
    String dua_body;
    String dua_reference;
    String duaAudioId;

    TextView duaTitle, duaBody, duaReference;
    ImageButton closeBtn;
    ImageButton playButton;
    MediaPlayer mediaPlayer;
    public Dua dua;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dhikri_detail);

        duaTitle = findViewById(R.id.duaTitle);
        duaBody = findViewById(R.id.duaTranslation);
        duaReference = findViewById(R.id.duaReference);

        closeBtn = findViewById(R.id.closeButton);
        playButton = findViewById(R.id.playButton);

//        mediaPlayer = new MediaPlayer();


        final Intent intent = getIntent();


        if (intent != null) {
            dua_title = intent.getStringExtra("dua_title");
            dua_body = intent.getStringExtra("dua_body");
            dua_reference = intent.getStringExtra("dua_reference");
            duaAudioId = String.valueOf(intent.getIntExtra("dua_audio_id", 1));


            duaTitle.setText(dua_title);
            duaBody.setText(dua_body);


            if (dua_reference.isEmpty()) {

                duaReference.setVisibility(View.GONE);

            } else {
                duaReference.setVisibility(View.VISIBLE);
                duaReference.setText(dua_reference);
            }
        }

        closeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        String jsonInString = null;
        if (intent != null) {
            jsonInString = intent.getStringExtra("dua");
        }


        Gson gson = new Gson();

        Dua dua = gson.fromJson(jsonInString, Dua.class);

        final Uri musicUri = Uri.parse("android.resource://" + getPackageName() + "/" + dua.getAudioResourceId());

        final MediaPlayer mp = new MediaPlayer();
//        Play Button Handler
        playButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


//                try {
//                    mediaPlayer.setDataSource(getApplicationContext(), musicUri);
//                    mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
//                        @Override
//                        public void onPrepared(MediaPlayer mp) {
//                            mediaPlayer.start();
//                            playButton.setBackgroundResource(R.drawable.pause_icon);
//                        }
//                    });
//
//                    mediaPlayer.prepare();
//
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//
//                mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
//                    @Override
//                    public void onCompletion(MediaPlayer mp) {
//                        //mediaPlayer.release();
//                        playButton.setBackgroundResource(R.drawable.icon_play);
//                    }
//                });
//
//
//                if (mediaPlayer.isPlaying()) {
//                    mediaPlayer.pause();
//                } else {
//                    mediaPlayer.start();
//                }



                //set up MediaPlayer


                try {
                    mp.setDataSource(getApplicationContext(),musicUri);
                    mp.prepare();
                    mp.start();
                    playButton.setBackgroundResource(R.drawable.pause_icon);
                } catch (Exception e) {
                    e.printStackTrace();
                }



                mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        //mediaPlayer.release();
                        playButton.setBackgroundResource(R.drawable.icon_play);
                    }
                });



            }

        });




//                String duaString = intent.("dua");
//                Gson gson = new Gson();
//                dua = gson.fromJson(duaString, Dua.class);
//                Uri myUri = Uri.parse("android.resource://+"+getPackageName()+"/raw/"+dua.getAudioResourceId());
//
//                try {
//                    mediaPlayer.setDataSource(getApplicationContext(), myUri);
//                    mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
//                        @Override
//                        public void onPrepared(MediaPlayer mp) {
//                            mediaPlayer.start();
//                        }
//                    });
//
//                    mediaPlayer.prepare();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }

//                if (PlayAudioService.isPlaying()) {
//                    PlayAudioService.Pause();
//                } else {
//                    mediaPlayer.start();
//                }


//                mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
//                    @Override
//                    public void onCompletion(MediaPlayer mp) {
//                        mediaPlayer.reset();
//                    }
//                });


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        mediaPlayer.stop();
    }
}
