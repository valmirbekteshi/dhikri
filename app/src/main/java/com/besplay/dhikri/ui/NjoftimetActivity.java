package com.besplay.dhikri.ui;

import android.annotation.TargetApi;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.besplay.dhikri.R;
import com.besplay.dhikri.helpers.LocalData;


import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class NjoftimetActivity extends AppCompatActivity {


    ImageButton toolbarBackButton;
    ImageButton clockButton;
    ImageButton clockButton1;
    CheckBox checkBoxMengjes;
    CheckBox checkBoxMbremje;

    LocalData localData;

    public static final String MyPREFERENCES = "MyPrefs";
    AlertDialog alertDialog;
    public static final int DAILY_REMINDER_REQUEST_CODE1 = 109;
    public static final int DAILY_REMINDER_REQUEST_CODE2 = 109;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_njoftimet);

        final Calendar calendar = Calendar.getInstance();

        localData = new LocalData(getApplicationContext());

        Typeface ralewayRegular = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Raleway-Regular.ttf");
        Typeface ralewayBold = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Raleway-Bold.ttf");

        clockButton = (ImageButton) findViewById(R.id.clock);
        clockButton1 = (ImageButton) findViewById(R.id.clock1);

        checkBoxMengjes = (CheckBox) findViewById(R.id.checkBoxMengjes);
        checkBoxMbremje = (CheckBox) findViewById(R.id.checkBoxMbremje);

        final TextView dhikriMengjesitTitle = (TextView) findViewById(R.id.dhikriMengjesitTitle);
        final TextView dhikriMbremjesTitle = (TextView) findViewById(R.id.dhikriMbremjesTitle);
        TextView aktivizoTitle1 = (TextView) findViewById(R.id.aktivizoTitle1);
        TextView aktivizoTitle2 = (TextView) findViewById(R.id.aktivizoTitle2);
        TextView toolbarTitle = (TextView) findViewById(R.id.toolbarTitle);

        final TextView alarm1 = (TextView) findViewById(R.id.alarm1);
        TextView alarm2 = (TextView) findViewById(R.id.alarm2);

        dhikriMbremjesTitle.setTypeface(ralewayRegular);
        dhikriMbremjesTitle.setTypeface(ralewayRegular);
        aktivizoTitle1.setTypeface(ralewayRegular);
        aktivizoTitle2.setTypeface(ralewayRegular);
        toolbarTitle.setTypeface(ralewayBold);
        alarm1.setTypeface(ralewayRegular);
        alarm2.setTypeface(ralewayRegular);

        toolbarBackButton = (ImageButton) findViewById(R.id.toolbarBackButton);

        toolbarBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });



        if (localData.getAmReminderStatus()) {
            clockButton.setAlpha(1f);
            clockButton.setEnabled(true);


        } else {
            clockButton.setAlpha(0.1f);
            clockButton.setEnabled(false);
            dhikriMengjesitTitle.setAlpha(0.1f);
        }


        if (localData.getPmReminderStatus()) {
            clockButton1.setAlpha(1f);
            clockButton1.setEnabled(true);


        } else {
            clockButton1.setAlpha(0.1f);
            clockButton1.setEnabled(false);
            dhikriMbremjesTitle.setAlpha(0.1f);
        }


        final TextView alarmText = (TextView) findViewById(R.id.alarmText1);
        final TextView alarmText1 = (TextView) findViewById(R.id.alarmText2);

        alarmText.setTypeface(ralewayRegular);
        alarmText1.setTypeface(ralewayRegular);




        final int getHour = localData.get_AmHour();
        final int getMin = localData.get_AmMin();


        final int getHour1 = localData.get_PmHour();
        final int getMin1 = localData.get_PmMin();



        alarmText.setText(getFormatedTime(getHour,getMin));


        alarmText1.setText(getFormatedTime(getHour1,getMin1));

        checkBoxMengjes.setChecked(localData.getAmReminderStatus());
        checkBoxMbremje.setChecked(localData.getPmReminderStatus());


        checkBoxMengjes.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                localData.setAmReminderStatus(isChecked);
                if (isChecked) {

                    dhikriMengjesitTitle.setAlpha(1f);
                    clockButton.setAlpha(1f);
                    clockButton.setEnabled(true);

                    //if checked get value from sharedPrefs
                    setNotificationAM(NjoftimetActivity.this, getHour, getMin);

                } else {

                    int id = localData.getAmNotificationId();
                    cancelNotification(id);
                    //cancelAMNotification();


                    dhikriMengjesitTitle.setAlpha(0.25f);
                    clockButton.setAlpha(0.25f);
                    clockButton.setEnabled(false);


                }
            }
        });


        checkBoxMbremje.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                localData.setPmReminderStatus(isChecked);
                if (isChecked) {

                    dhikriMbremjesTitle.setAlpha(1f);
                    clockButton1.setAlpha(1f);
                    clockButton1.setEnabled(true);

                    setNotificationPM(NjoftimetActivity.this, getHour1, getMin1);

                } else {

                   int id = localData.getPmNotificationId();
                   cancelNotification(id);


                    //cancelPMNotification();

                    dhikriMbremjesTitle.setAlpha(0.25f);
                    clockButton1.setAlpha(0.25f);
                    clockButton1.setEnabled(false);
                }
            }
        });


        clockButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                alertDialog = new AlertDialog.Builder(NjoftimetActivity.this).create();
                LayoutInflater inflater = NjoftimetActivity.this.getLayoutInflater();
                final View inflateView = inflater.inflate(R.layout.time_picker_spinner, null);


                Typeface ralewayRegular = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Raleway-Regular.ttf");

                final TimePicker tp = (TimePicker) inflateView.findViewById(R.id.timePicker);
                tp.setCurrentHour(calendar.get(Calendar.HOUR));
                tp.setCurrentMinute(calendar.get(Calendar.MINUTE));





                Button cancelBtn = (Button) inflateView.findViewById(R.id.cancelBtn);
                cancelBtn.setTypeface(ralewayRegular);
                cancelBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialog.cancel();
                    }
                });


                Button setBtn = (Button) inflateView.findViewById(R.id.setBtn);

                setBtn.setTypeface(ralewayRegular);
                TextView txtTitle = (TextView) inflateView.findViewById(R.id.txtTitle);
                txtTitle.setTypeface(ralewayRegular);


                setBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        int hour = tp.getCurrentHour();
                        int min = tp.getCurrentMinute();

                        setNotificationAM(getApplicationContext(), hour, min);


                        localData.set_AmHour(hour);
                        localData.set_AmMin(min);





                        alarmText.setText(getFormatedTime(hour,min));


                        alertDialog.cancel();

                    }
                });


                alertDialog.setView(inflateView);
                alertDialog.show();

            }
        });


        clockButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                alertDialog = new AlertDialog.Builder(NjoftimetActivity.this).create();
                LayoutInflater inflater = NjoftimetActivity.this.getLayoutInflater();
                final View inflateView = inflater.inflate(R.layout.time_picker_spinner, null);


                Typeface ralewayRegular = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Raleway-Regular.ttf");


                final TimePicker tp = (TimePicker) inflateView.findViewById(R.id.timePicker);


                tp.setCurrentHour(calendar.get(Calendar.HOUR));
                tp.setCurrentMinute(calendar.get(Calendar.MINUTE));




                Button cancelBtn = (Button) inflateView.findViewById(R.id.cancelBtn);

                cancelBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialog.cancel();
                    }
                });
                cancelBtn.setTypeface(ralewayRegular);


                Button setBtn = (Button) inflateView.findViewById(R.id.setBtn);

                setBtn.setTypeface(ralewayRegular);
                TextView txtTitle = (TextView) inflateView.findViewById(R.id.txtTitle);
                txtTitle.setTypeface(ralewayRegular);




                setBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        int hour = tp.getCurrentHour();
                        int min = tp.getCurrentMinute();


                        setNotificationPM(getApplicationContext(), hour, min);


                        localData.set_PmHour(hour);
                        localData.set_PmMin(min);


                        alarmText1.setText(getFormatedTime(hour,min));

                        alertDialog.cancel();

                    }
                });

                alertDialog.setView(inflateView);
                alertDialog.show();

            }
        });

    }




    public void setNotificationAM(Context context, int hour, int minute) {

        AlarmManager alarmManager = (AlarmManager)getSystemService(ALARM_SERVICE);
        Calendar beforeCalendar = Calendar.getInstance();

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, hour);
        calendar.set(Calendar.MINUTE, minute);
        calendar.set(Calendar.SECOND, 0);


        if (calendar.before(beforeCalendar)) {

            calendar.add(Calendar.DATE, 1);
        }

        Intent intent = new Intent("DISPLAY_NOTIFICATION_1");


        //int icon = R.drawable.icon2;

        //String koha = "Mëngjesit";
        //
        //intent.putExtra("icon", icon);
        //intent.putExtra("koha", koha);
        //intent.putExtra("title", "DHIKRI I MËNGJESIT");
       // intent.setAction("AM");

        int id = (int) System.currentTimeMillis();



        //cancel already scheduled notification
        //cancelNotification(id);

        localData.setAmNotificationId(id);

        PendingIntent broadcast = PendingIntent.getBroadcast(getApplicationContext(),100,
            intent,PendingIntent.FLAG_UPDATE_CURRENT);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            alarmManager.setRepeating(AlarmManager.RTC_WAKEUP,calendar.getTimeInMillis(),AlarmManager.INTERVAL_DAY,broadcast);
        }

    }


    public void setNotificationPM(Context context, int hour, int minute) {
        AlarmManager alarmManager = (AlarmManager)getSystemService(ALARM_SERVICE);
        Calendar beforeCalendar = Calendar.getInstance();

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, hour);
        calendar.set(Calendar.MINUTE, minute);
        calendar.set(Calendar.SECOND, 0);


        if (calendar.before(beforeCalendar)) {

            calendar.add(Calendar.DATE, 1);
        }

        Intent intent = new Intent("DISPLAY_NOTIFICATION_2");

        //
        //int icon = R.drawable.icon11;
        //
        //String koha = "Mbrëmjes";
        //
        ////intent.putExtra("icon", icon);
        ////intent.putExtra("koha", koha);
        ////intent.putExtra("title", "DHIKRI I MBRËMJES");
        //intent.setAction("PM");

        int id = (int) System.currentTimeMillis();



        //cancel already scheduled notification
        //cancelNotification(id);

        localData.setAmNotificationId(id);




        PendingIntent broadcast = PendingIntent.getBroadcast(NjoftimetActivity.this,1032,
            intent,PendingIntent.FLAG_UPDATE_CURRENT);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            alarmManager.setRepeating(AlarmManager.RTC_WAKEUP,calendar.getTimeInMillis(),AlarmManager.INTERVAL_DAY,broadcast);
        }

    }

    public void cancelNotification(int id) {
        //Intent intent = new Intent(this, NotificationRecieverAM.class);
        //PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), id, intent, 0);
        //AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        //alarmManager.cancel(pendingIntent);
    }

    public void cancelAMNotification(){
        //PackageManager pm  = NjoftimetActivity.this.getPackageManager();
        //ComponentName componentName = new ComponentName(NjoftimetActivity.this, NotificationRecieverAM.class);
        //pm.setComponentEnabledSetting(componentName,PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
        //    PackageManager.DONT_KILL_APP);
        //Toast.makeText(getApplicationContext(), "cancelled am", Toast.LENGTH_LONG).show();
    }

    public void cancelPMNotification(){
        //PackageManager pm  = NjoftimetActivity.this.getPackageManager();
        //ComponentName componentName = new ComponentName(NjoftimetActivity.this, NotificationRecieverPM.class);
        //pm.setComponentEnabledSetting(componentName,PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
        //    PackageManager.DONT_KILL_APP);
        //Toast.makeText(getApplicationContext(), "cancelled pm", Toast.LENGTH_LONG).show();
    }


    public static void cancelReminder(Context context,Class<?> cls)
    {
        // Disable a receiver
        //ComponentName receiver = new ComponentName(context, cls);
        //PackageManager pm = context.getPackageManager();
        //
        //pm.setComponentEnabledSetting(receiver,
        //        PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
        //        PackageManager.DONT_KILL_APP);
        //
        //Intent intent1 = new Intent(context, cls);
        //PendingIntent pendingIntent = PendingIntent.getBroadcast(context, DAILY_REMINDER_REQUEST_CODE1, intent1, PendingIntent.FLAG_UPDATE_CURRENT);
        //AlarmManager am = (AlarmManager) context.getSystemService(ALARM_SERVICE);
        //am.cancel(pendingIntent);
        //pendingIntent.cancel();
    }



    public String getFormatedTime(int h, int m) {
        final String OLD_FORMAT = "HH:mm";
        final String NEW_FORMAT = "hh:mm a";

        String oldDateString = h + ":" + m;
        String newDateString = "";

        try {
            SimpleDateFormat sdf = new SimpleDateFormat(OLD_FORMAT, getCurrentLocale());
            Date d = sdf.parse(oldDateString);
            sdf.applyPattern(NEW_FORMAT);
            newDateString = sdf.format(d);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return newDateString;
    }


    @TargetApi(Build.VERSION_CODES.N)
    public Locale getCurrentLocale() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return getResources().getConfiguration().getLocales().get(0);
        } else {
            //noinspection deprecation
            return getResources().getConfiguration().locale;
        }
    }


}
