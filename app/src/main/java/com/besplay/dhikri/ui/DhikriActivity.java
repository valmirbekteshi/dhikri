package com.besplay.dhikri.ui;

import android.content.Intent;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.besplay.dhikri.DhikriDetail;
import com.besplay.dhikri.helpers.PlayAudioService;
import com.besplay.dhikri.R;
import com.besplay.dhikri.adapter.DuaAdapter;
import com.besplay.dhikri.helpers.RecyclerItemClickListener;
import com.besplay.dhikri.model.Dua;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import static com.besplay.dhikri.helpers.PlayAudioService.playAudioService;

public class DhikriActivity extends AppCompatActivity {
    ImageButton toolbarBackButton;
    ImageButton toolbarSettingsButton;


    private RecyclerView recyclerView;
    private ArrayList<Dua> duaList;
    private DuaAdapter adapter;
    public Dua dua;
    private MediaPlayer mMediaPlayer;
    private TextView toolbarTitle;
    private ProgressBar progressBar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dhikri);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        Typeface ralewayBold = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Raleway-Bold.ttf");

        toolbarBackButton = (ImageButton) findViewById(R.id.toolbarBackButton);
//        toolbarSettingsButton = (ImageButton) findViewById(R.id.toolbarSettingsButton);
        toolbarTitle = (TextView) findViewById(R.id.toolbarTitle);
        toolbarTitle.setTypeface(ralewayBold);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        Intent intent = getIntent();
        String title = intent.getStringExtra("title");
        toolbarTitle.setText(title);


        duaList = new ArrayList<>();

        setData();


        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new DuaAdapter(duaList, progressBar, getApplicationContext());
        recyclerView.setAdapter(adapter);



        toolbarBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                Intent i = new Intent(DhikriActivity.this, MainActivity.class);
                startActivity(i);
            }
        });


//        toolbarSettingsButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                Intent i = new Intent(DhikriActivity.this, NjoftimetActivity.class);
//                startActivity(i);
//
//            }
//        });


//        Intent i = new Intent(this, PlayAudioService.class);
//        startService(i);


        final LayoutInflater inflater = DhikriActivity.this.getLayoutInflater();
        View inflateView = inflater.inflate(R.layout.dua_list, null);
//        final ImageButton playPauseButton = (ImageButton) inflateView.findViewById(R.id.playPauseButton);

        mMediaPlayer = new MediaPlayer();


//        playPauseButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//
//                Intent intent = new Intent();
//
//                String duaString = intent.getStringExtra("dua");
//                Gson gson = new Gson();
//                dua = gson.fromJson(duaString, Dua.class);
//                Uri myUri = Uri.parse("android.resource://com.besplay.dhikri.ui/" + dua.getAudioResourceId());
//
//                try {
//                    mMediaPlayer.setDataSource(getApplicationContext(), myUri);
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//
//                if (PlayAudioService.isPlaying()) {
//                    PlayAudioService.Pause(playPauseButton);
//                } else {
//                    mMediaPlayer.start();
//                }
//
//
//                mMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
//                    @Override
//                    public void onCompletion(MediaPlayer mp) {
//                        mMediaPlayer.reset();
//                    }
//                });
//            }
//        });


        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getApplicationContext(), recyclerView,
                        new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {

                        Dua dua = duaList.get(position);

                        Gson gson = new Gson();
                        String json = gson.toJson(dua);

                        Intent intent = new Intent(DhikriActivity.this, DhikriDetail.class);
                        intent.putExtra("dua_title",duaList.get(position).getDuaTitle());
                        intent.putExtra("dua_body",duaList.get(position).getDuaTranslation());
                        intent.putExtra("dua_reference",duaList.get(position).getDuaReference());
                        intent.putExtra("dua_audio_id",duaList.get(position).getAudioResourceId());
                        intent.putExtra("dua",json);
                        startActivity(intent);

                    }

                    @Override
                    public void onLongItemClick(View view, int position) {

                    }
                })
        );




    }


    private List<Dua> setData() {

        duaList = new ArrayList<>();

        String dua1 = "Elhamdu lil-ahi vahdeh, ves-salatu ves -selamu ala men la nebijje baّdeh.";
        String duaTranslation1 = "Falënderimi i takon vetëm All - llahut, mëshira dhe shpëtimi(i All - llahut)qofshin mbi atë pas të cilit s`ka më Pejgamber, Muhammedin(s.a.v.s.)";
        String duaReference1 = "";

        String dua2 = "All-llahu La Ilahe il-la huvel-Hajjul-Kajjumu, la te`hudhuhu sinetun ve la nevmun, lehu ma fis-semavati ve ma fil-erdi, men dhel-ledhi jeshfeّu ّindehu il-la bi idhnihi jaّlemu ma bejne ejdihim ve ma halfehum ve la juhitune bi shej`in min ّilmihi il-la bima shae vesi'a Kursijjuhus-semavati vel-erda, ve la jeuduhu hifdhuhuma ve huvel-ّAlijjul-ّAdhim.";
        String duaTranslation2 = "All-llahu është Një, s`ka Zot tjetër përveç Atij, Ai është mbikëqyrës i përhershëm dhe i përjetshëm. Atë nuk e zë as kotja as gjumi; Gjithçka ka në qiej dhe në tokë është vetëm e Tij. Kush mund të ndërmjetësojë tek Ai, pos me lejen e Tij. Ai di të tashmen që është pranë tyre dhe të ardhmen. Nga ajo që Ai di, të tjerët dinë vetëm aq sa Ai ka dëshiruar. Kursija e Tij përfshin qiejt dhe tokën, kurse kujdesi i Tij ndaj të dyjave nuk i vjen rëndë, Ai është më i Larti, më i Madhi.";
        String duaReference2 = "";

        String dua3 = "Kul huvall-llahu ehad. All-llahus-Samed. Lem jelid ve lem juled. Ve lem jekun lehu kufuven ehad.";
        String duaTranslation3 = "Thuaj: Ai, All-llahu është Një! All-llahu është Ai që çdo krijesë i drejtohet (i mbështetet) për çdo nevojë. As s’ka lindur kë, as nuk është i lindur. Dhe Atij askush nuk i është i barabartë.";
        String duaReference3 = "(3 herë)";


        String dua4 = "Kul eudhu bi rabbil felek. Min sherri ma halek. Ve min sherri gasikin idha vekab. Ve min sherrin-nef-fathatin fil ukad. Ve min sherri hasedin idha hased.";
        String duaTranslation4 = "Thuaj: I mbështetem Zotit të agimit, Prej dëmit të çdo krijese, që Ai e krijoi. Dhe prej errësirës së natës kur ngryset plotësisht. Dhe prej dëmit të atyre që lidhin dhe fryejnë nyja. Edhe prej dëmit të smirëkeqit kur sipas smirës vepron.";
        String duaReference4 = "(3 herë)";

        String dua5 = " Kul eudhu bi rabbin-nas.Melikin-nas.ilahin-nas. Min sherril-vesvasil-hanas.El-ledhi juvesvisu fi sudurin-nas Minel-xhin-neti ven-nas.";
        String duaTranslation5 = "Thuaj: “Mbështetem (mbrohem) me Zotin e njerëzve! Sunduesin e njerëzve, Të adhuruarin e njerëzve, Prej të keqes së cytësit që fshihet. I cili bën cytje në zemrat e njerëzve, Qoftë ai (cytësi) nga xhinët ose nga njerëzit”.";
        String duaReference5 = "(3 herë)";


        String dua6 = "Asbahna ve asbaha el-Mulku lil-lah vel-hamdulil-lah la Ilahe il-lAll-llahu vahdehu la sherike leh, lehul-mulku ve lehul-hamdu ve Huve ّala kul-li shejin kadir. Rabbi es`eluke-l-hajre ma fi hadhel-jevmi ve hajre ma baّdehu ve eّudhu bike min sherri ma fi hadhel-jevmi ve sherri ma baّdehu. Rabbi eّudhu bike minel-keseli ve su`il-kiber. Rabbi eّudhu bike min ّadhabin fin-nar ve ّadhabin fil-kabr.";
        String duaTranslation6 = "All-llahu është Një, s`ka Zot tjetër përveç Atij, Ai është mbikëqyrës i përhershëm dhe i përjetshëm. Atë nuk e zë as kotja as gjumi; Gjithçka ka në qiej dhe në tokë është vetëm e Tij. Kush mund të ndërmjetësojë tek Ai, pos me lejen e Tij. Ai di të tashmen që është pranë tyre dhe të ardhmen. Nga ajo që Ai di, të tjerët dinë vetëm aq sa Ai ka dëshiruar. Kursija e Tij përfshin qiejt dhe tokën, kurse kujdesi i Tij ndaj të dyjave nuk i vjen rëndë, Ai është më i Larti, më i Madhi.";
        String duaReference6 = "Muslimi 4/2088.";

        String dua7 = "All-llahume bike esbahna ve bike emsejna ve bike nahja ve bike nemutu ve ilejken-nushur.";
        String duaTranslation7 = "O Zot im, me emrin Tënd e arrijmë mëngjesin dhe me emrin Tënd e arrijmë mbrëmjen, me emrin Tënd ngjallemi dhe me emrin Tënd vdesim dhe te Ti është kthimi.";
        String duaReference7 = "Ndërsa në mbrëmje\"All-llahume bike emsejna ve bike esbahna ve bike nahja ve bike nemutu ve ilejkel-mesir\" (O Zot, me emrin Tënd e arrijmë mbrëmjen dhe me emrin Tënd e arrijmë mëngjesin, me emrin Tënd ngjallemi dhe me emrin tënd vdesim dhe te Ti është tubimi).";

        String dua8 = "All-llahume ente Rabbi la Ilahe il-la ente halakteni ve ene ّabduke ve ene ّala ّahdike ve vaّdike mestet’atu eّudhu bike min sherri na sanaّtu eb`u leke bi niّmetike ّaleje ve eb`u bi dhenbi fagfir li fe innehu la jagfirudh-dhunube il-la Ente.";
        String duaTranslation8 = "O All-llah, ti je Zoti im, nuk ka të adhuruar tjetër përveç Teje; Ti më ke krijuar dhe unë jam robi Yt, do të qëndroj besnik ndaj marrëveshjes dhe premtimit Tënd, sa të kem mundësi; Kërkoj mbrojtjen Tënde nga e keqja që kam vepruar, Unë jam mirënjohës ndaj dhuntive Tua, i pranoj mëkatet e mia, andaj më fal, ngase mëkatet nuk i fal askush tjetër përveç Teje.";
        String duaReference8 = "Kush e thotë këtë në mbrëmje duke qenë i bindur në të dhe vdes po atë natë do të jetë prej banorëve të Xhennetit, poashtu edhe në mëngjes”. (Buhariu 7/150).";

        String dua9 = "All-llahumme inni asbahtu ush-hiduke, ve ush-hidu hamelete ّArshike ve Melaiketeke ve xhemiّa halkike, Enneke EnteAll-llahu la Ilahe il-la Ente vahdeke la sherike Lek, ve enne Muhammeden ّabduke ve resuluke (4 herë).";
        String duaTranslation9 = "O Zoti im, u gdhiva, dëshmoj për Ty dhe mbajtësit e Arshit Tënd, melaiket Tua dhe të gjitha krijesat Tua, se vërtetë Ti je All-llahu i Vetëm, s`ka të adhuruar tjetër përveç Teje, Ti je i Vetëm dhe i pashoq dhe dëshmoj se Muhammedi (s.a.v.s.) është rob dhe i dërguar i Yt.";
        String duaReference9 = "";

        String dua10 = "All-llahume ma asbaha bi min niْmetin ev bi ehadin min halkike, fe minke vahdeke la sherike lek, fe lekel-hamdu ve lekesh-shukru.";
        String duaTranslation10 = "O Zoti im, ajo që më është dhënë mua apo ndonjërit prej krijesave Tua nga begatitë e tëra është vetëm nga Ti që Je i pashoq; Ty të qoftë lavdërimi dhe falënderimi.";
        String duaReference10 = "Ndërsa në mbrëmje thotë: All-llahumme ma emsa bi…";

        String dua11 = "All-llahume ّafini fi bedeni, All-llahume ّafini fi semْi, All-llahume ّafini fi besari, la ilahe il-la ente, All-llahume inni eّudhu bike minel-kufri vel-fakri, ve eّudhu bike min adhabil-kabr, la ilahe il-la ente (tri herë në mëngjes dhe tri herë në mbrëmje).";
        String duaTranslation11 = "O Zoti im, më jep shëndet në trupin tim, O Zoti im, më jep shëndet në të dëgjuarit tim, O Zoti im, më jep shëndet në të pamurit tim. S'ka të adhuruar tjetër përveç Teje. O Zoti im, kërkoj mbrojtjen Tënde nga kufri (mosbesimi) e varfëria dhe kërkoj mbrojtjen Tënde nga dënimi i varrit. S'ka të adhuruar tjetër përveç Teje.";
        String duaReference11 = "Ebu Davudi 4/324, Ahmedi 5/42 dhe Nesaiu në “Amelul-jevmi vel-lejleh” nr:22, Ibën Sunnijj nr: 69 dhe Buhariu në “El-Edeb-El-Mufred”, senedin e këtij hadithi dijetari Ibën Bazi në librin e tij “Tuhfetul-Ahjar” fq.26 e ka bërë të mirë (hasen).";

        String dua12 = "HasbijAll-llahu la Ilahe il-la hu(ve), ّAlejhi tevekkeltu ve Huve Rabbul-`arshil-`adhim. (7 herë).";
        String duaTranslation12 = "Më mjafton mua All-llahu; s'ka të adhuruar tjetër përveç Tij. Tek Ai jam mbështetur dhe Ai është Zoti i Arshit të Madh.";
        String duaReference12 = "Kush e thotë këtë në mëngjes dhe në mbrëmje (7 herë) i mjafton për atë që e brengos çështja e kësaj bote dhe e ahiretit.\" (Ibën Sunnijj hadithi nr:71, merfu dhe Ebu Davudi mevkuf 4/321. Isnadin e këtij hadithi e kan bërë të vërtetë Shuajb dhe Abdulkadër Arnauti “Zadul-Me'ad” 2/376).";

        String dua13 = "All-llahume inni es`elukel-`afve vel-`afijete fid-dunja vel-ahireh, All-llahume inni es`elukel-`afve vel-afijete fi dini ve dunjaje ve ehli ve mali. All-llahumes-tur ّavrati ve amin revّati. All-llahume hfedhni min bejni jedejje ve min halfi ve ّan jemini ve ّan shimali ve min fevki ve eّudhu bike en ugtale min tahti.";
        String duaTranslation13 = "O Zoti im, kërkoj nga Ti falje dhe shpëtim në këtë botë dhe në Ahiret. Zoti im, kërkoj që të më falësh dhe të më mbrosh në fenë time dhe në jetën time, ma mbro familjen dhe pasurinë time. O Zoti im, m'i mbulo të metat e mia dhe më qetëso në momentet trishtuese. O Zot, më ruaj nga para dhe prapa, në të djathtë, në të majtë dhe nga lartë; kërkoj nga ti që të më mbrosh të mos më lëshojë toka.";
        String duaReference13 = "Ebu Davudi dhe Ibën Maxheh. Shih “Sahih Ibën Maxheh” 2/332.";

        String dua14 = "All-llahume ّalimel-gajbi vesh-shehadeti fatires-semavati vel-erdi, Rabbe kul-le shej`in ve melikehu, eshhedu en la Ilahe il-la ente, eّudhu bike min sherri nefsive sherrish-shejtani ve shirkihi ve en akterife ّala nefsi su`en ev exhurrehu ila muslimin.";
        String duaTranslation14 = "O Zoti im, Ti je Ai i Cili i di të fshehtat dhe të dukshmet, Krijues i qiejve dhe i tokës, Zot i çdo sendi dhe Mbizotërues i saj, dëshmoj se nuk ka të adhuruar tjetër përveç Teje; kërkoj mbrojtjen Tënde nga e keqja e vetes sime dhe nga e keqja e djallit dhe nga ajo që ai (shejtani) shpie në shirk dhe kërkoj të më mbrosh që vetvetes e as ndonjë muslimani të mos i bëj keq.";
        String duaReference14 = "Tirmidhiu dhe Ebu Davudi “Sahih Et-Tirmidhi” 3/142.";

        String dua15 = "Bismil-lahil-ledhi la jedur-ru me`a-ismihi shej`un fil-erdi ve la fis-semai ve huves-semiّul- ّalim (3 herë).";
        String duaTranslation15 = "Me emrin e All-llahut pranë emrit të të Cilit nuk bën dëm asgjë në tokë e as në qiell, Ai që dëgjon shumë dhe di çdo send.";
        String duaReference15 = "\"Kush e thotë këtë (3 herë) në mëngjes dhe në mbrëmje, nuk i bëhet dëm asgjë.\" Ebu Davudi 4/323, Tirmidhiu 5/465, Ibën Maxhe dhe Ahmedi. Shih “Sahih Ibën Maxhe” 2/332), Ibën Bazi në “Tuhfetul-Ahjar” fq.39, (senedi hasen).";

        String dua16 = "Redijtu bil-lahi Rabben ve bil-Islami dinen, ve bi Muhammedin \uF072 nebijjen.";
        String duaTranslation16 = "Jam i kënaqur që Zoti im është All-llahu, feja ime është Islami dhe Pejgamberi im Muhammedi(s.a.v.s).";
        String duaReference16 = "\"Kush e thotë këtë çdo mëngjes dhe mbrëmje tri herë është obligim i All-llahut që ta kënaq atë ditën e Kijametit.\" (Ahmedi 4/337, Nesaiu në “Amelul-jevmi vel-lejleh” nr:4, Ibën Sunnijj nr:68, Ebu Davudi 4/418 dhe Tirmidhiu 5/465. Ibën Bazi në “Tuhfetul-Ahjar” fq.39 (hadith hasen).";

        String dua17 = "Ja Hajju ja Kajjumu bi rahmetike estegithu aslih li she`ni kul-lehu ve la tekilni ila nefsi tarfete ajnin.";
        String duaTranslation17 = "O i Gjallë përgjithmonë, O Mbikëqyrës i çdo gjëje, me mëshirën Tënde kërkoj ndihmë, ma përmirëso tërë gjendjen time dhe mos më lë të mbështetem në veten time, as sa një lëvizje e syrit.";
        String duaReference17 = "Sahih sipas Hakim, e ka pëlqyer edhe Dhehebiu 1/545. Shih “Sahihut-tergib vet-terhib” 1/273.";

        String dua18 = "Asbahna ve asbahal-Mulku lil-lahi Rabbil-ْalemin. All-llahumme inni es`eluke hajre hadhel-jevmi fet-hahu, ve nasrehu ve nurehu, ve bereketehu, ve hudahu, ve eّudhu bike min sherri ma fihi, ve sherri ma baّdehu.";
        String duaTranslation18 = "E arritëm mëngjesin dhe e tërë pasuria i takon All-llahut, Zotit të botërave. O Zoti im, unë kërkoj mirësinë e kësaj dite, hapjen e saj, ndihmën e saj, dritën e saj, dhuntinë dhe udhëzimin e saj. Kërkoj të më mbrosh nga e keqja e saj dhe e ditëve të tjera pas saj.";
        String duaReference18 = "Ebu Davudi 4/322; (isnad hasen) Sh. dhe A. Arnauti. Shih “Zadul-Me'ad” 2/273.";

        String dua19 = "Asbahna ّala fitretil-Islam ve ّala kelimetil-Ihlas, ve ّala dini nebijjina Muhammedin \uF072, ve ّala mil-leti ebina Ibrahime hanifen muslimen ve ma kane minel-mushrikin.";
        String duaTranslation19 = "E arritëm mëngjesin në natyrshmërinë Islame, në fjalën e sinqertë (fjala: LA ILAHE IL-LALL-LLAH), në fenë e Pejgamberit tonë, Muhammedit \uF072 dhe në popullin (fenë) e babait tonë Ibrahimit, i cili ka qenë besimdrejtë, musliman e nuk ka qenë prej mushrikëve (idhujtarëve).";
        String duaReference19 = "Ahmedi 3/406-407, Ibën Sunnijj në “Amelul-jevmi vel-lejleh” nr: 34 “Sahih El-Xhami'u” 4/209.";

        String dua20 = "SubhanAll-llahi ve bihamdihi (100 herë).";
        String duaTranslation20 = "I Lartësuar qoftë All-llahu, Atij të Cilit i takon Lavdërimi.";
        String duaReference20 = "\"Kush e thotë këtë në mëngjes dhe në mbrëmje 100 herë, askush s'do të vijë Ditën e Kijametit me diç më të vlefshme se ky, përveç atij i cili ka thënë sikur ky apo më tepër.\" ( Muslimi 4/2071).";


        String dua21 = "La Ilahe il-lAll-llahu vahdehu la sherike leh, lehul-mulku ve lehul-hamdu ve huve ala kul-li shejin kadir.";
        String duaTranslation21 = "I Lartësuar qoftë All-llahu, Atij të Cilit i takon Lavdërimi.";
        String duaReference21 = "Ebu Davudi 4/319, Ibën Maxheh dhe Ahmedi 4/60, “Sahih Et-tergib vet-terhib” 1/270 dhe “Sahih Ebu Davud” 3/957, “Sahih Ibën Maxheh” 2/331 dhe “Zadul-Me'ad” 2/377.";

        String dua22 = "La ilahe il-lAll-llahu vahdehu la sherike leh, lehul-Mulku ve lehul-hamdu ve huve ala kul-li shejin kadir (100 herë në mëngjes).";
        String duaTranslation22 = "S'ka të adhuruar përveç All-llahut, Një dhe i pashoq, Atij i takon sundimi dhe lavdërimi. Ai është i plotfuqishëm mbi çdo send.";
        String duaReference22 = "Pejgamberi (s.a.v.s.) ka thënë: \"Kush e thotë këtë 100 herë në ditë; ka shpërblimin sikur t'i ketë liruar 10 robër. Atij i shkruhen 100 të mira, i fshihen 100 mëkate (gjynahe) dhe është i mbrojtur nga djalli deri në mbrëmje, askush nuk ka vepruar më mirë se ky person, me përjashtim të atij që ka vepruar më tepër\". (Buhariu 4/95 dhe Muslimi 4/2071).";

        String dua23 = "SubhanAll-llahi ve bihamdihi ّadede halkihi ve rida nefsihi ve zinete ّarshihi ve midade kelimatihi (tri herë në mëngjes).";
        String duaTranslation23 = "I Madhëruar qoftë All-llahu, aq sa është numri i krijesave të Tij dhe aq sa dëshiron Ai vet. Po aq sa është i bukur Arshi i Tij dhe sa ngjyra e pasosur (pafund) për t`i shkruar fjalët e Tij.";
        String duaReference23 = "Muslimi 4/2090.";

        String dua24 = "All-llahume inni es`eluke ّilmen nafi`an ve rizkan tajjiben ve ّamelen mutekabbelen (çdo mëngjes).";
        String duaTranslation24 = "O Zoti im, të lutem më jep dituri të dobishme e furnizim të mirë dhe të lutem Të m'i pranosh veprat e mia.";
        String duaReference24 = "Ibën Sunnijj në “Amelul-jevmi vel-lejleh” nr: 54 Ibën Maxheh nr: 925; Isnadin e këtij hadithi e kanë bërë të mirë (hasen) Sh. dhe A. Arnauti “Zadul-Me'ad” 2/375.";

        String dua25 = "Estagfirullahe ve etubu ilejhi (100 herë në ditë).";
        String duaTranslation25 = "Kërkoj faljen e All-llahut dhe tek Ai pendohem.";
        String duaReference25 = "Buhariu “Fet-hul-Bari” 11/101.";

        String dua26 = "E'udhu bi kelimatil-lahit-tammati min sherri ma halaka. (tre herë në mbrëmje).";
        String duaTranslation26 = "Kërkoj mbrojtje me fjalët e përsosura të All-llahut nga sherri (e keqja) që ka krijuar.";
        String duaReference26 = "\"Kush e thotë këtë (3 herë) në mbrëmje, nuk i bën dëm temperatura e asaj nate\". (Ahmedi 2/290, Nesaiu në \"Amelul-jevmi vel-lejleh\" hadithi nr:590, Ibën Sunnijj nr:68). “Sahih Et-Tirmidhi” 3/187, “Sahih Ibën Maxheh” 2/266 dhe “Tehfetul-Ahjar” fq.45.";

        String dua27 = "All-llahumme sal-li ve sel-lim ّala nebijjina Muhammed (10 herë).";
        String duaTranslation27 = "Kërkoj faljen e All-llahut dhe tek Ai pendohem.";
        String duaReference27 = "Buhariu “Fet-hul-Bari” 11/101.";


        duaList.add(new Dua(1, R.raw.dua_one, dua1, duaTranslation1, duaReference1, false));
        duaList.add(new Dua(2, R.raw.dua_two, dua2, duaTranslation2, duaReference2, false));
        duaList.add(new Dua(3, R.raw.dua_three_one, dua3, duaTranslation3, duaReference3, false));
        duaList.add(new Dua(4, R.raw.dua_three_two, dua4, duaTranslation4, duaReference4, false));
        duaList.add(new Dua(5, R.raw.dua_three_three, dua5, duaTranslation5, duaReference5, false));
        duaList.add(new Dua(6, R.raw.dua_four, dua6, duaTranslation6, duaReference6, false));
        duaList.add(new Dua(7, R.raw.dua_five, dua7, duaTranslation7, duaReference7, false));
        duaList.add(new Dua(8, R.raw.dua_six, dua8, duaTranslation8, duaReference8, false));
        duaList.add(new Dua(9, R.raw.dua_seven, dua9, duaTranslation9, duaReference9, false));
        duaList.add(new Dua(10, R.raw.dua_eight, dua10, duaTranslation10, duaReference10, false));
        duaList.add(new Dua(11, R.raw.dua_nine, dua11, duaTranslation11, duaReference11, false));
        duaList.add(new Dua(12, R.raw.dua_ten, dua12, duaTranslation12, duaReference12, false));
        duaList.add(new Dua(13, R.raw.dua_eleven, dua13, duaTranslation13, duaReference13, false));
        duaList.add(new Dua(14, R.raw.dua_twelve, dua14, duaTranslation14, duaReference14, false));
        duaList.add(new Dua(15, R.raw.dua_thirteen, dua15, duaTranslation15, duaReference15, false));
        duaList.add(new Dua(16, R.raw.dua_fourteen, dua16, duaTranslation16, duaReference16, false));
        duaList.add(new Dua(17, R.raw.dua_fifteen, dua17, duaTranslation17, duaReference17, false));
        duaList.add(new Dua(18, R.raw.dua_sixteen, dua18, duaTranslation18, duaReference18, false));
        duaList.add(new Dua(19, R.raw.dua_seventeen, dua19, duaTranslation19, duaReference19, false));
        duaList.add(new Dua(20, R.raw.dua_eighteen, dua20, duaTranslation20, duaReference20, false));
        duaList.add(new Dua(21, R.raw.dua_nineteen, dua21, duaTranslation21, duaReference21, false));
        duaList.add(new Dua(22, R.raw.dua_nineteenn, dua22, duaTranslation22, duaReference22, false));
        duaList.add(new Dua(23, R.raw.dua_twenty, dua23, duaTranslation23, duaReference23, false));
        duaList.add(new Dua(24, R.raw.dua_twentyone, dua24, duaTranslation24, duaReference24, false));
        duaList.add(new Dua(25, R.raw.dua_twentytwo, dua25, duaTranslation25, duaReference25, false));
        duaList.add(new Dua(26, R.raw.dua_twentythree, dua26, duaTranslation26, duaReference26, false));
        duaList.add(new Dua(27, R.raw.dua_twentyfour, dua27, duaTranslation27, duaReference27, false));

        return duaList;


    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        playAudioService.Pause();
        stopService(new Intent(this, PlayAudioService.class));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopService(new Intent(this, PlayAudioService.class));
    }
}
