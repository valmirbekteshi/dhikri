package com.besplay.dhikri.ui;

import android.app.ActivityOptions;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.besplay.dhikri.R;
import com.luseen.spacenavigation.SpaceItem;
import com.luseen.spacenavigation.SpaceNavigationView;

public class MainActivity extends AppCompatActivity {
    private LinearLayout layout1;
    private RelativeLayout layout2;
    private ImageView imageView;
    private TextView title1, title2, title3;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);


        Typeface relewaySemiBold = Typeface.createFromAsset(getAssets(), "fonts/Raleway-SemiBold.ttf");
        Typeface ubuntuBold = Typeface.createFromAsset(getAssets(), "fonts/Ubuntu-Bold.ttf");
        Typeface ubuntuLight = Typeface.createFromAsset(getAssets(), "fonts/Ubuntu-Light.ttf");
        Typeface ubuntuMedium = Typeface.createFromAsset(getAssets(), "fonts/Ubuntu-Medium.ttf");
        Typeface ubuntuRegular = Typeface.createFromAsset(getAssets(), "fonts/Ubuntu-Regular.ttf");
        Typeface ubuntuLightItalic = Typeface.createFromAsset(getAssets(), "fonts/Ubuntu-LightItalic.ttf");


        title1 = (TextView) findViewById(R.id.title1);
        title2 = (TextView) findViewById(R.id.title2);
//        title3 = (TextView) findViewById(R.id.title3);

        title1.setTypeface(ubuntuMedium);
        title2.setTypeface(ubuntuMedium);
//        title3.setTypeface(ubuntuMedium);

        layout1 =  (LinearLayout)findViewById(R.id.layout1);
        layout2 = (RelativeLayout) findViewById(R.id.layout2);
        //layout3 = (LinearLayout) findViewById(R.id.layout3);

        LayoutInflater inflater = MainActivity.this.getLayoutInflater();
        final View inflateView = inflater.inflate(R.layout.activity_dhikri, null);

        TextView toolbarTitle = (TextView) inflateView.findViewById(R.id.toolbarTitle);

        toolbarTitle.setText("TEST");

        SpaceNavigationView spaceNavigationView = (SpaceNavigationView) findViewById(R.id.space);
        spaceNavigationView.initWithSaveInstanceState(savedInstanceState);
        spaceNavigationView.addSpaceItem(new SpaceItem("FAV", R.drawable.fav_icon));
        spaceNavigationView.setCentreButtonIcon(R.drawable.home_icon);
        spaceNavigationView.addSpaceItem(new SpaceItem("SEARCH", R.drawable.settings_icon));
        spaceNavigationView.showIconOnly();


        layout1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(MainActivity.this, DhikriActivity.class);
                i.putExtra("title", "DHIKRI I MËNGJESIT");

                startActivity(i);
            }
        });




//        layout2.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//
//            }
//        });

//
//        layout3.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent i = new Intent(MainActivity.this, NjoftimetActivity.class);
//                startActivity(i);
//            }
//        });


    }

    public void morningDhikrOnClick(View view){
        Intent i = new Intent(MainActivity.this, DhikriActivity.class);
        i.putExtra("title", "DHIKRI I MBRËMJES");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            startActivity(i, ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
        }else {
            startActivity(i);
        }


    }

}
