package com.besplay.dhikri.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.besplay.dhikri.R;
import com.besplay.dhikri.model.Dua;

import java.util.List;

import static com.besplay.dhikri.helpers.PlayAudioService.playAudioService;

/**
 * Created by Valmir on 5/17/2017.
 */

public class DuaAdapter extends RecyclerView.Adapter<DuaAdapter.MyProductHolder> {


    private List<Dua> duaList;
    private Context context;
    private ProgressBar progressBar;

    public static class MyProductHolder extends RecyclerView.ViewHolder {

        private TextView duaTitle;
        private TextView duaTranslation;
        private TextView duaReference;
        //private ImageButton playPauseButton;


        public MyProductHolder(View itemView) {
            super(itemView);

            duaTitle = (TextView) itemView.findViewById(R.id.duaTitle);
            duaTranslation = (TextView) itemView.findViewById(R.id.duaTranslation);
            //duaReference = (TextView) itemView.findViewById(R.id.duaReference);
            //playPauseButton = (ImageButton) itemView.findViewById(R.id.playPauseButton);

        }
    }


    public DuaAdapter(List<Dua> productList, ProgressBar progressBar, Context context) {
        this.duaList = productList;
        this.context = context;
        this.progressBar = progressBar;
    }


    @Override
    public MyProductHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.dua_list, parent, false);
        return new MyProductHolder(itemView);
    }


    @Override
    public void onBindViewHolder(final MyProductHolder holder, final int position) {
        final Dua dua = duaList.get(position);

        Typeface ralewayRegular = Typeface.createFromAsset(context.getAssets(), "fonts/Raleway-Regular.ttf");
        Typeface ralewaySemiBold = Typeface.createFromAsset(context.getAssets(), "fonts/Raleway-SemiBold.ttf");


//        if (dua.isPressed()) {
//            holder.playPauseButton.setBackgroundResource(R.drawable.pause_icon);
//        } else {
//            holder.playPauseButton.setBackgroundResource(
//                    R.drawable.play_icon);
//        }
//
//        holder.playPauseButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//
//
//
//                //imageButton = holder.playPauseButton;
//
////
////                if (playAudioService.isPlaying())
////                {
////
////                    playAudioService.Pause();
////
////                }
////                else
////                {
////
////                    Uri myUri = Uri.parse("android.resource://com.besplay.dhikri/" + dua.getAudioResourceId());
////
////
////                    playAudioService.Play(duaList,dua, myUri, holder.playPauseButton,DuaAdapter.this);
////                }
//
//                Uri myUri = Uri.parse("android.resource://com.besplay.dhikri/" + dua.getAudioResourceId());
//                playAudioService.Play(duaList, dua, myUri, holder.playPauseButton, DuaAdapter.this);
//
//
////                Intent i = new Intent(context, DhikriActivity.class);
////                Gson gson = new Gson();
////                i.putExtra("dua", gson.toJson(dua));
////                context.startService(i);
//            }
//        });



        holder.duaTitle.setText(dua.getDuaTitle());
        holder.duaTranslation.setText(dua.getDuaTranslation());
        //holder.duaReference.setText(dua.getDuaReference());

    }


    @Override
    public int getItemCount() {
        if (duaList != null) {
            return duaList.size();
        }
        return 0;
    }

}



