package com.besplay.dhikri.reciever;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.provider.Settings;
import android.support.v4.app.NotificationCompat;
import com.besplay.dhikri.R;
import com.besplay.dhikri.ui.DhikriActivity;

/**
 * Created by Valmir on 19-Mar-18.
 */

public class AlarmReciever1 extends BroadcastReceiver {

  @Override public void onReceive(Context context, Intent intent) {

    NotificationManager notificationManager =
        (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
    
    Intent notificationIntent = new Intent(context, DhikriActivity.class);
    notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);  // FIXME: 25-Mar-18

    TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
    stackBuilder.addParentStack(DhikriActivity.class);
    stackBuilder.addNextIntent(notificationIntent);

    String koha = "Koha për Dhikrin e Mëngjesit";
    int icon = intent.getIntExtra("icon", R.drawable.icon2);
    String title = "Dhkri i Mëngjesit";
    notificationIntent.putExtra("title", title);

    PendingIntent pendingIntent =
        stackBuilder.getPendingIntent(100, PendingIntent.FLAG_UPDATE_CURRENT);

    NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
        .setContentTitle(title)
        .setContentText(koha)
        .setSmallIcon(icon)
        .setAutoCancel(true)
        .setSmallIcon(R.drawable.icon2)
        .setVibrate(new long[] { 500, 500,1000})
        .setSound(Settings.System.DEFAULT_NOTIFICATION_URI)
        .setContentIntent(pendingIntent);


    notificationManager.notify((int) System.currentTimeMillis(), builder.build());
  }
}
